import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FrodoTest {
    @Test
    public void testUpdate_Version() {
        Frodo frodo = new Frodo();
        frodo.setName("Frodo");
        assertThat(frodo.getName()).isEqualTo("Frodo");
    }
}